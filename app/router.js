import {Router} from 'express';
import Dumper from './dumper';
import zlib from 'zlib';
import intoStream from 'into-stream';
const route = new Router();
const dumper = new Dumper('opun', 'opun', 'test2.syrexcloud.com:5984');

route.get('/dump/:db', (req, res) => {
  res.setHeader('content-encoding', 'gzip');
  res.setHeader('content-type', 'application/json');

  dumper.readDump(req.params.db, req.query.callback).then((data) => intoStream(data).pipe(zlib.createGzip()).pipe(res));
});

route.post('/dump/:db', (req, res) => res.send(dumper.startSync(req.params.db)));
route.patch('/dump/:db', (req, res) => res.send(dumper.writeDump(req.params.db)));
route.delete('/dump/:db', (req, res) => res.send(dumper.stopSync(req.params.db)));
route.options('/dump/:db', (req, res) => res.send(dumper.dumpStatus(req.params.db)));

export default route;
