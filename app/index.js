import path from 'path';
import logger from 'morgan';
import express from 'express';
import bodyParser from 'body-parser';
import cookieParser from 'cookie-parser';
import routes from './router';
const app = express();

app.use('/', routes);
app.use(logger('dev'));
app.use(cookieParser());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(express.static(path.join(__dirname, 'share')));

app.use((req, res) => {
  const err = new Error('Not Found');
  err.status = 404;
  res.send(JSON.stringify(err));
});

app.use((err, req, res) => {
  res.status(err.status || 500);
  res.send(JSON.stringify(err));
});

app.listen(5445, () => console.log('Started on port: 5445'));
