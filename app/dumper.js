import PouchDB from 'pouchdb';
import fs from 'fs';
import replicationStream from 'pouchdb-replication-stream';

PouchDB.plugin(replicationStream.plugin);
PouchDB.adapter('writableStream', replicationStream.adapters.writableStream);
PouchDB.debug.enable('*');

function readFile(name) {
  return new Promise((resolve, reject) => {
    const stream = fs.createReadStream(name, {encoding: 'utf8'});
    const result = [];
    const onRead = () => {
      let buf = stream.read();
      while (buf !== null) {
        result.push(buf);
        buf = stream.read();
      }
    };

    stream.on('readable', onRead)
      .once('end', () => resolve(result.join('')))
      .once('error', error => reject(error));
  });
}

export default class Dumper {
  constructor(user, pass, host) {
    this.server = `http://${user}:${pass}@${host}/`;
    this.shared = 'shared/';
    this.instanses = {
      local: {},
      remote: {},
      sync: {},
      status: {}
    };

    try {
      fs.accessSync(this.shared);
    } catch (err) {
      fs.mkdirSync(this.shared);
    }
  }

  setListeners(db) {
    this.instanses.sync[db]
      .on('paused', () => this.writeDump(db, 'paused'))
      .on('change', () => this.writeDump(db, 'change'))
      .on('active', () => this.writeDump(db, 'active'))
      .on('complete', () => this.writeDump(db, 'complete'))
      .on('error', (err) => console.log(err));
  }

  linkToRemote(db) {
    this.instanses.remote[db] = new PouchDB(this.server + db);
    return this.instanses.remote[db];
  }

  linkToLocal(db) {
    this.instanses.local[db] = new PouchDB(this.shared + db);
    return this.instanses.local[db];
  }

  dumpStatus(db) {
    return this.instanses.status[db] && {status: this.instanses.status[db]} || {status: 'dump_missed'};
  }

  startSync(db) {
    const local = this.linkToLocal(db);
    const remote = this.linkToRemote(db);
    this.instanses.sync[db] = local.replicate.from(remote, {live: true, retry: true});
    this.setListeners(db);
    return {status: 'sync_run'};
  }

  stopSync(db) {
    this.instanses.sync[db].cancel();
    delete this.instanses.sync[db];
    delete this.instanses.local[db];
    delete this.instanses.remote[db];
    delete this.instanses.status[db];
    return {status: 'sync_term'};
  }

  writeDump(db, status = 'default') {
    const ws = fs.createWriteStream(this.shared + db + '.data');
    this.instanses.remote[db].dump(ws).then((res) => {
      if (res.ok) {
        this.instanses.status[db] = 'dump_available';
        console.log('Dump has written in ' + status);
      }
    });
  }

  readDump(db, callbackName) {
    const toJSONP = chunks => `/**/${callbackName}(${JSON.stringify({chunks})});`;
    return readFile(this.shared + db + '.data').then(chunks => toJSONP(chunks));
  }
}
